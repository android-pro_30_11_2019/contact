package com.example.contact.Adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contact.Entity.Contact;
import com.example.contact.R;

public class ContactViewHolder extends RecyclerView.ViewHolder {
    private TextView tvDisplayName;
    private ContactListener listener;

    public ContactViewHolder(@NonNull View itemView, final ContactListener listener) {
        super(itemView);
        tvDisplayName = itemView.findViewById(R.id.tvDisplayName);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onRecylerViewClick(getAdapterPosition());
            }
        });
    }

    public void onBind(Contact contact) {
        tvDisplayName.setText(contact.getDisplay_name());
    }
}
