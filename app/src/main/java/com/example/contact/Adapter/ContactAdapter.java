package com.example.contact.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.contact.Entity.Contact;
import com.example.contact.R;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {
    List<Contact> contacts;
    ContactListener listener;

    public ContactAdapter() {
        new ArrayList<>();
    }
    public void setOnClickListener(ContactListener listener){
        this.listener = listener;
    }

    @NonNull
    @Override
    public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_contact,parent,false);
        return new ContactViewHolder(view,listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {
        holder.onBind(contacts.get(position));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }
    public void addMoreItem(List<Contact> contacts){
        this.contacts = contacts;
        notifyDataSetChanged();
    }
}
